from django.contrib import admin
from address_book.models import UserIBAN


@admin.register(UserIBAN)
class UserIBANAdmin(admin.ModelAdmin):

    def iban_number_last_4_digits(self, user_iban):
        return str(user_iban.iban)

    def iban_number(self, user_iban):
        return user_iban.iban.decrypt()

    def get_list_display(self, request):
        list_display = list(super().get_list_display(request) + ('nickname', 'user', 'iban_number_last_4_digits'))
        if request.user.is_superuser:
            list_display.append('iban_number')
        return list_display
