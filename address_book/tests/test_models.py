from django.test import TestCase

from address_book.models import UserAddress
from address_book.factory import UserFactory


class UserAddressTestCase(TestCase):

    def test_duplicate_addresses_are_removed(self):
        """" dedup_user_address: Check if duplicate addresses are successfully removed """
        # Given
        user = UserFactory.create()

        add1 = UserAddress(name="Max", city="Giventown", user=user)
        add2 = UserAddress(name="Max Mustermann", street_address="Randomstreet", city="Giventown", user=user)
        add3 = UserAddress(name="Max Mustermann", street_address="456 Randomstreet", city="Giventown", user=user)
        add4 = UserAddress(name="Max Mustermann", street_address="789 Otherstreet", city="Giventown", country="NL", user=user)

        # When
        add1.save()
        add2.save()
        add3.save()
        add4.save()

        # Then
        self.assertEquals(UserAddress.objects.all().count(), 2)
        self.assertTrue(UserAddress.objects.filter(id=add3.id).exists())
        self.assertTrue(UserAddress.objects.filter(id=add4.id).exists())
        self.assertFalse(UserAddress.objects.filter(id=add1.id).exists())
        self.assertFalse(UserAddress.objects.filter(id=add2.id).exists())


# TODO: Add tests for UserIBAN model. Gonna skip it due to time constraint
# My strategy would be to inject mocked encryption manager and write tests against that.
