from django.contrib.auth.models import User

import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('email')
    email = factory.Faker('email')
