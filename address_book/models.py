from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver

from django_mysql.models import Model
from address_book.fields import IBANField


class UserAddress(Model):
    """ User Address Store """

    user = models.ForeignKey(User, on_delete='cascade')
    name = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255)
    street_address_line2 = models.CharField(max_length=255, blank=True, null=True)
    zipcode = models.CharField(max_length=12, blank=True, null=True)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=64, blank=True, null=True)
    country = models.CharField(max_length=2)
    full_address = models.TextField(blank=True)
   
    def save(self, *args, **kwargs):
        streetdata = f"{self.street_address}\n{self.street_address_line2}"
        self.full_address = f"{streetdata}\n{self.zipcode} {self.city} {self.state} {self.country}"
        super().save(*args, **kwargs)

@receiver(pre_save, sender=UserAddress)
def dedup_user_address(sender, instance, **kwargs):
    """ removes duplicate user address on pre_save """
    user_addresses = UserAddress.objects.raw("SELECT * from address_book_useraddress where %s LIKE CONCAT('%%', full_address, '%%') and user_id=%s", [instance.full_address, instance.user_id])
    for user_address in user_addresses:
        user_address.delete()

class UserIBAN(Model):

    """ Secure place to store user IBANs """

    user = models.ForeignKey(User, on_delete='cascade')
    nickname = models.CharField(max_length=255)
    iban = IBANField()
