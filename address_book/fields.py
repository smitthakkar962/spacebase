import base64

from django.core.exceptions import ValidationError
from django.conf import settings

from django_mysql import models

import aws_encryption_sdk
import botocore
from schwifty import IBAN


class BaseEncryptionManager:

    def encrypt(self):
        raise NotImplementedError

class AWSEncryptionManager(BaseEncryptionManager):
    """ Encrypts the data using AWS encryption sdk. Uses CMKs stored in KMS """

    def _get_boto_session(self):
        return botocore.session.Session(profile=settings.AWS_PROFILE_NAME)
    
    @property
    def kms_key_provider(self):
        return aws_encryption_sdk.KMSMasterKeyProvider(key_ids=settings.AWS_CMK_ARNS, botocore_session=self._get_boto_session())

    def encrypt(self, value):
        return aws_encryption_sdk.encrypt(source=value, key_provider=self.kms_key_provider)[0]

    def decrypt(self, value):
        return aws_encryption_sdk.decrypt(source=value, key_provider=self.kms_key_provider)[0]

class IBANData:
    """ Object Representation of data stored in IBANField """

    def __init__(self, value, encryption_manager):
        self.encryption_manager = encryption_manager
        self.preview = value['preview']
        self.encrypted_iban = base64.b64decode(value['encrypted_iban'])

    def decrypt(self):
        return self.encryption_manager().decrypt(self.encrypted_iban).decode()
    
    def __str__(self):
        return f'---{self.preview}'
    
    def __repr__(self):
        return f'IBANData("---{self.preview}")'

class IBANField(models.JSONField):

    """ Custom field to store IBAN numbers. """

    def __init__(self, *args, encryption_manager=AWSEncryptionManager, **kwargs):
        self.encryption_manager = encryption_manager
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        value = super(IBANField, self).to_python(value)
        return IBANData(value, self.encryption_manager)

    def from_db_value(self, value, *args, **kwargs):
        value = super(IBANField, self).from_db_value(value, *args, **kwargs)
        return IBANData(value, self.encryption_manager)

    def pre_save(self, model_instance, add):
        value = super(IBANField, self).pre_save(model_instance, add)
        encrypted_iban = self.encryption_manager().encrypt(value)
        value = {
            'preview': IBAN(value).compact[-4:],
            'encrypted_iban': base64.b64encode(encrypted_iban).decode(),
        }
        setattr(model_instance, self.attname, value)
        return value

    def clean(self, value, model_instance):
        try:
            IBAN(value)
        except ValueError as iban_exception:
            raise ValidationError(iban_exception, code='invalid_iban_number')

        return super(PhoneNumberField, self).clean(value, model_instance)
