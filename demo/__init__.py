# pylint:disable=trailing-newlines
from urllib.parse import urlencode

import dj_database_url


def get_database_dict(db_url):
    return dj_database_url.parse(db_url)
