## Required env vars

- SPACEBASE_DATABASE_URL=<mysql_db_url>
- SPACEBASE_AWS_CMK_ARNS=<arn_for_cmk>
- SPACEBASE_AWS_DEFAULT_PROFILE=<aws_profile_name>
